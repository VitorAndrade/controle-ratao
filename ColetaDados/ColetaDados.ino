#define PWMA 2
#define AIN1 16
#define AIN2 4
#define STBY 17
#define BIN1 5
#define BIN2 18
#define PWMB 19

int MTA = 0, MTB = 0, dado = 0, aux = 0, dT = 10, encoder_esquerdo = 0, encoder_direito = 0;

const unsigned char pinEncoders[4] = {15,21,22,23};
volatile long contEncEsq = 0;                          
volatile long contEncDir = 0;
int tempo = 0;

void EncoderEsquerdoA() {
  if (digitalRead(pinEncoders[1]) != digitalRead(pinEncoders[0])) {
    contEncEsq ++;
  } else {
    contEncEsq --;
  }
}

void EncoderEsquerdoB() {
  if (digitalRead(pinEncoders[1]) == digitalRead(pinEncoders[0])) {
    contEncEsq ++;
  } else {
    contEncEsq --;
  }
}

void EncoderDireitoA() {
  if (digitalRead(pinEncoders[3]) != digitalRead(pinEncoders[2])) {
    contEncDir ++;
  } else {
    contEncDir --;
  }
}

void EncoderDireitoB() {
  if (digitalRead(pinEncoders[3]) == digitalRead(pinEncoders[2])) {
    contEncDir ++;
  } else {
    contEncDir --;
  }
}






void setup() {
  // put your setup code here, to run once:

  char data = 'b';
  
  for(unsigned char i = 0; i < 4; i++) {
    pinMode(pinEncoders[i], INPUT_PULLUP);
  }
  attachInterrupt(digitalPinToInterrupt(pinEncoders[0]), EncoderEsquerdoA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(pinEncoders[1]), EncoderEsquerdoB, CHANGE);
  attachInterrupt(digitalPinToInterrupt(pinEncoders[2]), EncoderDireitoA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(pinEncoders[3]), EncoderDireitoB, CHANGE);

      digitalWrite(AIN1,HIGH);
      digitalWrite(AIN2,LOW);
      digitalWrite(BIN1,LOW);
      digitalWrite(BIN2,HIGH);
  
  MTA = 2000;
  MTB = 2000;


  
 // pinMode(PWMA,OUTPUT);
  pinMode(AIN1,OUTPUT);
  pinMode(AIN2,OUTPUT);
  pinMode(STBY,OUTPUT);
  pinMode(BIN1,OUTPUT);
  pinMode(BIN2,OUTPUT);
 // pinMode(PWMB,OUTPUT);

 ledcAttachPin(PWMA, 0);
 ledcAttachPin(PWMB, 1);

 ledcSetup(0, 20000, 12);
 ledcSetup(1, 20000, 12);
  
  digitalWrite(STBY,HIGH);
  
  Serial.begin(9600);

  delay(1000);

  Serial.println('n');

  while(data != 'n')
  {
    data = Serial.read();
  }  
}

void loop() {               //falta um modo de obter a leitura dos encoders
  
 ledcWrite(0,MTA);
 ledcWrite(1,MTB);
 
  if(Serial.available() > 0)
  {
    dado = Serial.read();
    aux = 1;
  }

  
    if(millis() - tempo == dT)
    {
       if(aux == 1)
       {
          Serial.println(dado);
         // delay(100);
          Serial.println(encoder_esquerdo);
         // delay(100);
          Serial.println(encoder_direito);
         // delay(100);
          MTA = dado;
          MTB = dado;
       }   
       tempo = millis();
    }
  }
