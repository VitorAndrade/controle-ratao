// Controle de velocidade
boolean speedControl = true;
boolean kalmanEncoders = true;
float maxSpeed = 4.0;      // Em m/s
float diametroRoda = 0.03; // Em metros
long lastLoop = 0;

#include <SimpleKalmanFilter.h>

SimpleKalmanFilter leftEncKalman (0.3, 2, 0.1);
SimpleKalmanFilter rightEncKalman(0.3, 2, 0.1);

// Encoders
const unsigned char pinEncoders[4] = {15, 21, 22, 23}; // As duas primeiras portas corresponde as portas A e B do primeiro encoder enquanto
volatile long contEncEsq = 0;                          // as as duas portas seguintes corresponde as portas A e B do segundo encoder
volatile long contEncDir = 0;
long lastContEncEsq = 0;
unsigned long lastTimeEsq = 0;
unsigned long lastTimeDir = 0;
float velEncEsq = 0;
long lastContEncDir = 0;
float velEncDir = 0;
float realLeftSpeed = 0;
float realRightSpeed = 0;

// Encoders

void encEsqA() {
  if (digitalRead(pinEncoders[1]) != digitalRead(pinEncoders[0])) {
    contEncEsq ++;
  } else {
    contEncEsq --;
  }
}

void encEsqB() {
  if (digitalRead(pinEncoders[1]) == digitalRead(pinEncoders[0])) {
    contEncEsq ++;
  } else {
    contEncEsq --;
  }
}

void encDirA() {
  if (digitalRead(pinEncoders[3]) != digitalRead(pinEncoders[2])) {
    contEncDir ++;
  } else {
    contEncDir --;
  }
}

void encDirB() {
  if (digitalRead(pinEncoders[3]) == digitalRead(pinEncoders[2])) {
    contEncDir ++;
  } else {
    contEncDir --;
  }
}

void updateLeftEncoder() {
  long dX = contEncEsq - lastContEncEsq;
  long dT = micros() - lastTimeEsq;
  if (dX == 0) {
    if (dT >= 20000 && dX == 0) {
      if(kalmanEncoders) velEncEsq = leftEncKalman.updateEstimate(0);
      else velEncEsq = 0;
    } else if(kalmanEncoders) velEncEsq = leftEncKalman.updateEstimate(realLeftSpeed);
    return;
  }
  velEncEsq = dX * 1000000.0 / (120.0 * dT);
  realLeftSpeed = velEncEsq;
  if(kalmanEncoders) velEncEsq = leftEncKalman.updateEstimate(velEncEsq);
  lastContEncEsq = contEncEsq;
  lastTimeEsq = micros();
}

void updateRightEncoder() {
  long dX = contEncDir - lastContEncDir;
  long dT = micros() - lastTimeDir;
  if (dX == 0) {
    if (dT >= 20000 && dX == 0) {
      if(kalmanEncoders) velEncDir = rightEncKalman.updateEstimate(0);
      else velEncDir = 0;
    } else if(kalmanEncoders) velEncDir = rightEncKalman.updateEstimate(realRightSpeed); 
    return;
  }
  velEncDir = dX * 1000000.0 / (120.0 * dT);
  realRightSpeed = velEncDir;
  if(kalmanEncoders) velEncDir = rightEncKalman.updateEstimate(velEncDir);
  lastContEncDir = contEncDir;
  lastTimeDir = micros();
}

void updateEncoders() {
  updateLeftEncoder();
  updateRightEncoder();
}
               
void setup() {
  Serial.begin(115200);
  for (unsigned char i = 0; i < 4; i++) pinMode(pinEncoders[i], INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pinEncoders[0]), encEsqA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(pinEncoders[1]), encEsqB, CHANGE);
  attachInterrupt(digitalPinToInterrupt(pinEncoders[2]), encDirA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(pinEncoders[3]), encDirB, CHANGE);
  lastTimeEsq = micros();
  lastTimeDir = micros();
  lastLoop = micros();
}

void loop() {
  updateEncoders();
  Serial.print(realLeftSpeed);
  Serial.print(" ");
  Serial.println(velEncDir);
  if(micros() - lastLoop > 1000) {
    lastLoop = micros();
    
  } 
}
