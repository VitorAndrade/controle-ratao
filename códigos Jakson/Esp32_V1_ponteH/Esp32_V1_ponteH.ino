// All analog sensors
const unsigned char tam = 5;
const unsigned char portas[tam] = {26, 27, 14, 12, 13};//, 15, 2, 4}; //{36, 39, 34, 35, 32, 33, 25}; //, 26, 27, 14, 12, 13, 15, 2, 4};

//Define the Motors Pins

//Motor 1
int pinAIN1 = 16; //15; //5;  // Direction
int pinAIN2 = 4;  //8;  //4;  // Direction
int pinPWMA = 2;  //0;  //32; //3; // Speed

//Motor 2
int pinBIN1 = 5;  //0;  //7; // Direction
int pinBIN2 = 18; //4;  //8; // Direction
int pinPWMB = 19; //25; //9; // Speed

//Standby
int pinSTBY = 17; //2; //6;

//Constants to help remember the parameters of the motors
bool turnCW = false;
bool turnCCW = true;
bool motor1 = false;
bool motor2 = true;

//Sensors right and left
bool lastEsq = false;
bool flagEsq = false;
bool lastDir = false;
bool flagDir = false;

void MotorsPWM(int m2, int m1) {   // Função que recebe os valores PWM para ambos motores, direito e esquerdo
  bool sentido1;                   // O valor negativo inverte o sentido de rotação do motor
  bool sentido2;
  m1 = -m1;                        // Pela forma que o robô foi montado, se mandarmos o mesmo PWM para ambos motores eles girarão em sentidos opostos, por isso deve-se inverter o sentido de um motor multiplicando por -1
  if(m1 < 0) {
    sentido1 = HIGH;
    m1 = -m1;
  } else sentido1 = LOW;
  
  if(m2 < 0) {
    sentido2 = HIGH;
    m2 = -m2;
  } else sentido2 = LOW;

  if(m1 > 255) m1 = 255;
  if(m2 > 255) m2 = 255;
  
  // Motor 1
  digitalWrite(pinAIN1, sentido1);
  digitalWrite(pinAIN2, !sentido1);  //This is the opposite of the AIN1
  //analogWrite(pinPWMA, m1);  // PWM do arduino
  ledcWrite(0, m1);            // PWM do esp32
  
  // Motor 2
  digitalWrite(pinBIN1, sentido2);
  digitalWrite(pinBIN2, !sentido2);  //This is the opposite of the BIN1
  //analogWrite(pinPWMB, m2);  // PWM do arduino
  ledcWrite(1, m2);            // PWM do esp32
   
  // Finally , make sure STBY is disabled - pull it HIGH
  digitalWrite(pinSTBY, HIGH);
}

void setup() {
  Serial.begin(115200); //taxa de comunicação baud-rate

  for(unsigned char i = 0; i < tam; i++) pinMode(portas[i], INPUT);
  
  // Pinagem da ponte H
  pinMode(pinPWMA, OUTPUT);
  pinMode(pinAIN1, OUTPUT);
  pinMode(pinAIN2, OUTPUT);

  pinMode(pinPWMB, OUTPUT);
  pinMode(pinBIN1, OUTPUT);
  pinMode(pinBIN2, OUTPUT);

  pinMode(pinSTBY, OUTPUT);

  // Funções que fazem o papel de setar PWM no esp32
  ledcAttachPin(pinPWMA, 0); // Declara-se o pino e o canal referente a
  ledcAttachPin(pinPWMB, 1); // esse pino

  ledcSetup(0, 5000, 8);     // Para cada canal declarado deve-se setar o canal, a frequência dos pulsos por segundo
  ledcSetup(1, 5000, 8);     // e a "resolução" do PWM. O valor de 8 bits corresponde um valor de PWM de 0 a 255
}

void loop() {
  //MotorsPWM(80, 80);
  for(int i = -255; i < 256; i++) {
    MotorsPWM(i, i);
    delay(15);
  }
  for(int i = 255; i >= -255; i--) {
    MotorsPWM(i, i);
    delay(15);
  }
  /*
  for(unsigned char i = 0; i < tam; i++) {
    Serial.print(analogRead(portas[i]));
    Serial.print(" ");
  }
  Serial.println("\t");
  delay(40);
  
  for(int i = 0; i < 256; i++) {
    MotorsPWM(i, i);
    delay(15);
  }
  for(int i = 255; i >= 0; i--) {
    MotorsPWM(i, i);
    delay(15);
  }
  
  for(int i = -255; i < 256; i++) {
    MotorsPWM(i, i);
    delay(15);
  }
  for(int i = 255; i >= -255; i--) {
    MotorsPWM(i, i);
    delay(15);
  }
  */
}
