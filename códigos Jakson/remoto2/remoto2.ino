/*  07/09/2019
     Código de testes para o controle por infravermelho, ao pressionar o botão '1', '2' ou '3', ele alterna para a estratégia selecionada e dá um feedback auditivo,
     simulando uma situação real em que não possuímos acesso ao monitor serial;
*/


//#include <IRremote.h>

//int RECV_PIN = 36;
//int valor  = 0;

int strategy = 1;

void chooseStrategy () {

  while (analogRead(14) < 3000)
  {
    Serial.println(analogRead(14));
    if ( analogRead(36) < 1500) {
      strategy = 1;
      Serial.println("Estratégia 1 selecionada");
      ledcWriteTone(0, 3500);
      delay(600);
      ledcWriteTone(0, 0);
    }
    if ( analogRead(39) < 1500) {
      strategy = 2;
      Serial.println("Estratégia 2 selecionada");
      ledcWriteTone(0, 3000);
      delay(150);
      ledcWriteTone(0, 0);
      delay(300);
      ledcWriteTone(0, 3000);
      delay(150);
      ledcWriteTone(0, 0);
    }
    if ( analogRead(33) < 1500) {
      strategy = 3;
      Serial.println("Estratégia 3 selecionada");
      ledcWriteTone(0, 1500);
      delay(150);
      ledcWriteTone(0, 0);
      delay(300);
      ledcWriteTone(0, 1500);
      delay(150);
      ledcWriteTone(0, 0);
      delay(300);
      ledcWriteTone(0, 1500);
      delay(150);
      ledcWriteTone(0, 0);
    }
    if ( analogRead(25) < 1500) {
      strategy = 4;
      Serial.println("Estratégia 4 selecionada");
      ledcWriteTone(0, 1500);
      delay(2000);
      ledcWriteTone(0, 0);
    }

    delay(1);
  }


}

//IRrecv irrecv(RECV_PIN);

//decode_results results;

void setup()
{
  Serial.begin(9600);

  ledcSetup(0, 500, 8);
  ledcAttachPin(13, 0);

  if (true)chooseStrategy ();


}

void loop()
{
  Serial.println(strategy);
}
