/*  31/05/2019
 *  Com sensores laterais, lógica de detectar cruzamentos
 *  e alternar o controle PID nas retas e curvas.  
 *  Controle PID novo e funcional
 *  Velocidade em percentual
 *  Identifica se saiu da linha
 */

// Encoders

#define tam 25

unsigned char pinEncoders[4] = {15, 21, 23, 22}; //{13, 21, 15, 7};
volatile bool leituraEncoder[2][2]; // primeiro índice: 0 - encoder esquerdo e 1 - encoder direito
                                    // segundo índice: 0 - porta A e 1 - porta B
long contEncEsq = 0;
long contEncDir = 0;
long lastcontEncEsq = 0;
unsigned long lastTime = 0;
float velEncEsq = 0;

int velMedM1[tam];

// Estratégia

short velPID = 0;
const int colunas = 3; 
float matEstrategia[3][colunas] = {{15.0, 23.0},  // Velocidade
                                   {2   , 1   },  // Tipo de PID
                                   {1   , 7   }}; // Contador esquerdo, negativo contador direito

// Sair da linha
boolean saiuDaLinha = false;
unsigned int erroMaximo = 600000;
unsigned long tempoErro = 0;

// Buzzer
int freq = 2000;
int channel = 3;
int resolution = 8;
unsigned char pinBuzzer = 14;
unsigned long timeBuzzer = 0;

// Condicao de parada
bool terminou = false;
unsigned int tempoLimite = 50000;

// Leds e botao
const unsigned char led = 27;
const unsigned char bt = 13;

// Controle PID

const unsigned char  resolucaoPWM = 12;
const unsigned int   freqPWM      = 20000;
const int            maxPWM       = 4095;
const float          ajuste       = 100.0 * maxPWM / 255.0;

struct Control { // Estrutura para salvar os controles de PID e velocidade
  float Kp = 0;
  float Ki = 0;
  float Kd = 0;
  float vel = 0;

  void setControl(float p, float i, float d, float v) { // Função para setar os parâmetros de PID e velocidade
    Kp = p;
    Ki = i;
    Kd = d;
    vel = v;
  }

  void setControl(float p, float i, float d) {        // Função para setar os parâmetros de PID
    Kp = p;
    Ki = i;
    Kd = d;
    vel = 0;
  }
};

Control reta;  // Controle para reta
Control curva; // Controle para curva
Control pidVel;

unsigned long inicio = 0;

bool condicao = false;   // "condição" é uma variável utilizada para se alternar o controle entre reta e curva
bool cruzamento = false; // "cruzamento" é uma variável utilizada para verificar se há cruzamento (cruzamento == true) ou não (cruzamento == false)

int last_proportional = 0; // Variável que armazena o último erro (proportional), o qual varia entre -2000 e 2000
long integral = 0;         // Somatório de todos erros (proportional)

// Cruzamento e sensores laterais

// Pinos laterais analógicos
const unsigned char anaEsq = 25; // Pino sensor lateral esquerdo
const unsigned char anaDir = 36; // Pino sensor lateral direito

//Sensors laterais
unsigned short avgEsq = 20;
unsigned short avgDir = 500;

bool lastEsq = false;
bool flagEsq = false;
bool lastDir = false;
bool flagDir = false;

int contEsq = 0;
int contDir = 0;

// Sensores frontais e calibracao
const unsigned char numSensors = 5;

unsigned int maxMin[numSensors][2];  // 0 -> Max
                                     // 1 -> Min
                      
unsigned char portas[numSensors] = {39, 34, 35, 32, 33};
unsigned int sensors[numSensors];
unsigned int amplitude = 1000;
boolean calibrado = false;

// Sensores laterais

void retaCurva(int c) {    // Função usada para alternar o valor da condição entre true e false, a partir do valor do contador esquerdo (contEsq)
  condicao = true;
  if(c%2 == 0) condicao = false;
}

void detectaCruzamento() { // Esta função é usada
  if(analogRead(anaEsq) < avgEsq && analogRead(anaDir) < avgDir) {
    cruzamento = true;
  } else {
    cruzamento = false; 
    //digitalWrite(13, HIGH);               // Acender o led
  }
}

void sensorEsquerdo() { // Função que verifica se sensor esquerdo passou sobre a faixa lateral esquerda, caso ocorra contesq++
  lastEsq = flagEsq;
  int leituraEsquerda = analogRead(anaEsq);    // A leitura digital retorna 0 para branco e 1 para preto, A2 é a porta analógica do arduino onde se encontra o sensor lateral esquerdo
  
  if(leituraEsquerda < avgEsq) flagEsq = true;    // Se a leitura do sensor lateral for igual a zero significa que o robô leu a faixa a lateral, logo a flagEsq recebe true
  else flagEsq = false;                            // caso contrário recebe false

  if(flagEsq && !lastEsq) {                        // Se o robô leu a faixa lateral branca mas anteriormente não o tinha lido, tem de ser adicionado mais um ao contador esquerdo.
    //tempoEsq = millis();
    //detectaCruzamento();
    if(!cruzamento) {
      contEsq++;
      estrategia(contEsq);
      timeBuzzer = millis();
      ledcWrite(channel, 255);
      retaCurva(contEsq);                   // Função usada para alteranar a velocidade entre reta e curva
      Serial.print("Esquerdo: ");
      Serial.println(contEsq);
    }
    else Serial.println("Cruzamento");
  }

  if(!flagEsq && lastEsq) {
    
   }
}

void sensorDireito() { // Funcão análoga a função sensoresquerdo()
  lastDir = flagDir;
  int leituraDireita = analogRead(anaDir);
  
  if(leituraDireita < avgDir) flagDir = true;
  else flagDir = false;

  if(flagDir && !lastDir)  {
    //detectaCruzamento();
    if(!cruzamento) {
      contDir++;
      estrategia(-contDir);
      //timeBuzzer = millis();
      //ledcWrite(channel, 255);
      Serial.print("Direito: ") ;
      Serial.println(contDir);
    } else Serial.println("Cruzamento");
  }

  if(!flagDir && lastDir) {
    
  }
}

// Ponte H, PWM

//Define the Motors Pins

//Motor 1
int pinAIN1 = 16; //15; //5; // Direction
int pinAIN2 = 4; //8;  //4; // Direction
int pinPWMA = 2; //0 para o esp32 antigo; //32; //3; // Speed

//Motor 2
int pinBIN1 = 5;  //0;  //7; // Direction
int pinBIN2 = 18; //4;  //8; // Direction
int pinPWMB = 19; //25; //9; // Speed

//Standby
int pinSTBY = 17; //2; //6;

//Constants to help remember the parameters of the motors
bool turnCW = false;
bool turnCCW = true;
bool motor1 = false;
bool motor2 = true;

int mapeamento(int x, int in_min, int in_max, int out_min, int out_max) { // Função semelhante a função map();
  if(in_max != in_min) return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
  else return out_min;
}

float mapFloat(float x, float in_min, float in_max, float out_min, float out_max) {
  if (in_max != in_min) return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
  else return out_min;
}

void inicializaMaxMin(unsigned int max = 0, unsigned int min = 90000) { // Para inicializar o vetor maxMin
  for(unsigned char i = 0; i < numSensors; i++) {
    maxMin[i][0] = max;
    maxMin[i][1] = min;
  }
}

void calibrar() { // Para cada sensor deve salvar de sua leitura os valores máximos e mínimos
  for(unsigned char i = 0; i < numSensors; i++) {
    if(analogRead(portas[i]) > maxMin[i][0]) maxMin[i][0] = analogRead(portas[i]); // 0 -> Max
    if(analogRead(portas[i]) < maxMin[i][1]) maxMin[i][1] = analogRead(portas[i]); // 1 -> Min
  }
  calibrado = true;
}

void calibrarMedia(int *somatorio, unsigned int tempo, unsigned char num) { // Para cada sensor deve salvar de sua leitura os valores máximos e mínimos
  int value;
  for(unsigned char i = 0; i < numSensors; i++) somatorio[i] = 0; // Inicializa o vetor com 0
  for(unsigned char i = 0; i < num; i++) {                        // Soma num vezes as leituras dos sensores
    for(unsigned char i = 0; i < numSensors; i++) somatorio[i] += analogRead(portas[i]);
    delay(tempo);
  }
  for(unsigned char i = 0; i < numSensors; i++) {
    value = somatorio[i]/num;
    if(value > maxMin[i][0]) maxMin[i][0] = value; // 0 -> Max
    if(value < maxMin[i][1]) maxMin[i][1] = value; // 1 -> Min
  }
  calibrado = true;
}

void calibrarMedia(unsigned int vezes = 1, unsigned int tempo = 10, unsigned char num = 10) { // Para cada sensor deve salvar de sua leitura os valores máximos e mínimos
  int somatorio[numSensors];
  inicializaMaxMin();
  for(unsigned char i = 0; i < vezes; i++) calibrarMedia(somatorio, tempo, num);
}

void calibrar(unsigned int numVezes, unsigned int tempo = 10) {
  inicializaMaxMin();
  for(int i = 0; i < numVezes; i++) {
    calibrar();
    delay(tempo);
  }
  calibrado = true;
}

void read_sensors(unsigned int *_sensors) { // Função para fazer a leitura dos sensores frontais
  for(unsigned char i = 0; i < numSensors; i++) {
    int leitura = analogRead(portas[i]);
    if(calibrado) {      // Caso já esteja calibrado, se a leitura atual do sensor de porta[i] estiver no intervalo entre seu de menor e maior valor correspondente,
      if(leitura > maxMin[i][1] && leitura < maxMin[i][0]) _sensors[i] = mapeamento(leitura, maxMin[i][1], maxMin[i][0], 0, amplitude); // deve-se mapear o valor no 
      else {             // intervalo entre 0 e a amplitude
        if(leitura < maxMin[i][1]) _sensors[i] = 0; // Caso contrário deve-se salvar 0 mínimo se a leitura for menor que o valor mínimo
        else if(leitura > maxMin[i][0]) _sensors[i] = amplitude;               // ou salvar o valor da amplitude caso a leitura tenha valor maior
      }
    } else _sensors[i] = mapeamento(leitura, 0, 4095, 0, amplitude);
  }
}

int readLine(unsigned int *_sensors, boolean mode = false) {
    unsigned long avr = 0;
    unsigned int soma = 0, max = 0, min = amplitude;
    boolean on_line = false;
    static int last_value = 0;
    int value[numSensors];
    read_sensors(_sensors);
    for(unsigned char i = 0; i < numSensors; i++) {
      value[i] = _sensors[i];
      if(mode)  value[i] = amplitude - value[i];
      if(value[i] > max) max = value[i];
      if(value[i] < min) min = value[i];
      if(value[i] > amplitude*0.5) on_line = true;
    }
    if(!on_line) {
      if(last_value < (numSensors-1)*500) return 0;
      else return (numSensors-1)*1000;
    } else {
      for(unsigned char i = 0; i < numSensors; i++) {
        //value[i] = mapeamento(value[i], min, max, 0, amplitude);
        avr  += 1000*i*value[i];
        soma += value[i];
      }
      last_value = avr/soma;
    }
    return last_value;
}

void segueLinha(float p, float i, float d, float vel) {          // Função que faz o controle do robô, a qual recebe os parâmetros de PId e velocidade
    unsigned int position = readLine(sensors, true);             // A variável position recebe o valor da posição do robô em relação a linha, variando entre 0 e 4000. Pela configuração atual o robô faz a leitura da sua posição em relação a uma linha branca em uma pista preta, caso queira que o robô leia uma linha preta sobre uma pista branca => unsigned int position = qtra.readLine(sensors); 
    int proportional = position - 2000;                          // A variável proportional armazena o erro que vai de -2000 a 2000, uma vez que position varia entre 0 e 4000
    int derivative = proportional - last_proportional;           // A variável derivative recebe a variação do erro

    if((proportional <= 0 && last_proportional >= 0) || (proportional >= 0 && last_proportional <= 0) || abs(proportional) == 2000) { // Se o robô estiver sobre a linha (proportional == 0) ou se ele passar sobre ela, a variável integral que é um somatório de todos os erros
      if(millis() - tempoErro > 3000) {
        integral = 0;
        tempoErro = millis();
      }                                                                                                                   // deve receber 0
    }

    integral += proportional;            // integral é o somatório dos erros
    last_proportional = proportional;    // last_proportional recebe o último erro

    float power_difference = ((proportional*p + integral*i + derivative*d) * ajuste) / (float) maxPWM; // A variável power_difference recebe o valor de correção da trajetória do robô em termos de velocidade,
                                                                                                       // a qual será adicionado em um motor e subtraído do outro

    MotorsPWMPercentual(vel + power_difference, vel - power_difference);  // Função para setar as velocidades e sentidos de rotação para ambos motores
}

void segueLinha(Control c) {                                    // Função que faz o controle do robô, a qual deve receber uma variável do tipo Control na qual há os parâmetros de controle
    unsigned int position = readLine(sensors, true);            // A variável position recebe o valor da posição do robô em relação a linha, variando entre 0 e 4000. Pela configuração atual o robô faz a leitura da sua posição em relação a uma linha branca em uma pista preta, caso queira que o robô leia uma linha uma linha preta sobre uma pista branca => unsigned int position = qtra.readLine(sensors); 
    int proportional = position - 2000;                         // A variável proportional armazena o erro que vai de -2000 a 2000, uma vez que position varia entre 0 e 4000
    int derivative = proportional - last_proportional;          // A variável derivative recebe a variação do erro

    if((proportional <= 0 && last_proportional >= 0) || (proportional >= 0 && last_proportional <= 0) || abs(proportional) == 2000) { // Se o robô estiver sobre a linha (proportional == 0) ou se ele passar sobre ela, a variável integral que é um somatório de todos os erros
      if(millis() - tempoErro > 3000) {
        integral = 0;
        tempoErro = millis();
      }                                                                                                                     // deve receber 0
    }

    integral += proportional;            // integral é o somatório dos erros
    last_proportional = proportional;    // last_proportional recebe o último erro

    float power_difference = ((proportional*c.Kp + integral*c.Ki + derivative*c.Kd) * ajuste) / (float) maxPWM; // A variável power_difference recebe o valor de correção da trajetória do robô em termos de velocidade,
                                                                                                                // a qual será adicionado em um motor e subtraído do outro
    
    MotorsPWMPercentual(c.vel + power_difference, c.vel - power_difference);  // Função para setar as velocidades e sentidos de rotação para ambos motores
}

void segueLinha(Control c, float vel) {                           // Função que faz o controle do robô, a qual deve receber uma variável do tipo Control e uma para velocidade
    unsigned int position = readLine(sensors, true);              // A variável position recebe o valor da posição do robô em relação a linha, variando entre 0 e 4000. Pela configuração atual o robô faz a leitura da sua posição em relação a uma linha branca em uma pista preta, caso queira que o robô leia uma linha uma linha preta sobre uma pista branca => unsigned int position = qtra.readLine(sensors); 
    int proportional = position - 2000;                           // A variável proportional armazena o erro que vai de -2000 a 2000, uma vez que position varia entre 0 e 4000
    int derivative = proportional - last_proportional;            // A variável derivative recebe a variação do erro

    if((proportional <= 0 && last_proportional >= 0) || (proportional >= 0 && last_proportional <= 0) || abs(proportional) == 2000) { // Se o robô estiver sobre a linha (proportional == 0) ou se ele passar sobre ela, a variável integral que é um somatório de todos os erros
      if(millis() - tempoErro > 3000) {
        integral = 0;
        tempoErro = millis();
      }
    }

    integral += proportional;            // integral é o somatório dos erros
    last_proportional = proportional;    // lastproportional recebe o últio erro

    float power_difference = ((proportional*c.Kp + integral*c.Ki + derivative*c.Kd) * ajuste) / (float) maxPWM; // A variável power_difference recebe o valor de correção da trajetória do robô em termos de velocidade,
                                                                                                                // a qual será adicionado em um motor e subtraído do outro
    
    MotorsPWMPercentual(vel + power_difference, vel - power_difference);  // Função para setar as velocidades e sentidos de rotação para ambos motores
}

void MotorsPWMPercentual(float m2, float m1) {   // Função que recebe os valores PWM para ambos motores, direito e esquerdo (valores entre -100.0 e 100.0)
  bool sentido1;                                 // O valor negativo inverte o sentido de rotação do motor
  bool sentido2;
  m1 = -m1;                                      // Pela forma que o robô foi montado, se mandarmos o mesmo PWM para ambos motores eles girarão em sentidos opostos, por isso deve-se inverter o sentido de um motor multiplicando por -1
  m1 = round(mapFloat(m1, -100.0, 100.0, -maxPWM, maxPWM));
  m2 = round(mapFloat(m2, -100.0, 100.0, -maxPWM, maxPWM));
  if(m1 < 0) {
    sentido1 = HIGH;
    m1 = -m1;
  } else sentido1 = LOW;
  
  if(m2 < 0) {
    sentido2 = HIGH;
    m2 = -m2;
  } else sentido2 = LOW;

  if(m1 > maxPWM) m1 = maxPWM;
  if(m2 > maxPWM) m2 = maxPWM;
  
  // Motor 1
  digitalWrite(pinAIN1, sentido1);
  digitalWrite(pinAIN2, !sentido1);  //This is the opposite of the AIN1
  //analogWrite(pinPWMA, m1);  // PWM do arduino
  ledcWrite(0, m1);            // PWM do esp32
  
  // Motor 2
  digitalWrite(pinBIN1, sentido2);
  digitalWrite(pinBIN2, !sentido2);  //This is the opposite of the BIN1
  //analogWrite(pinPWMB, m2);  // PWM do arduino
  ledcWrite(1, m2);            // PWM do esp32
   
  // Finally , make sure STBY is disabled - pull it HIGH
  digitalWrite(pinSTBY, HIGH);
}

void MotorsPWM(int m2, int m1) {   // Função que recebe os valores PWM para ambos motores, direito e esquerdo
  bool sentido1;                   // O valor negativo inverte o sentido de rotação do motor
  bool sentido2;
  m1 = -m1;                        // Pela forma que o robô foi montado, se mandarmos o mesmo PWM para ambos motores eles girarão em sentidos opostos, por isso deve-se inverter o sentido de um motor multiplicando por -1
  if(m1 < 0) {
    sentido1 = HIGH;
    m1 = -m1;
  } else sentido1 = LOW;
  
  if(m2 < 0) {
    sentido2 = HIGH;
    m2 = -m2;
  } else sentido2 = LOW;

  if(m1 > maxPWM) m1 = maxPWM;
  if(m2 > maxPWM) m2 = maxPWM;
  
  // Motor 1
  digitalWrite(pinAIN1, sentido1);
  digitalWrite(pinAIN2, !sentido1);  // This is the opposite of the AIN1
  //analogWrite(pinPWMA, m1);        // PWM do arduino
  ledcWrite(0, m1);                  // PWM do esp32
  
  // Motor 2
  digitalWrite(pinBIN1, sentido2);
  digitalWrite(pinBIN2, !sentido2);  // This is the opposite of the BIN1
  //analogWrite(pinPWMB, m2);        // PWM do arduino
  ledcWrite(1, m2);                  // PWM do esp32
   
  // Finally , make sure STBY is disabled - pull it HIGH
  digitalWrite(pinSTBY, HIGH);
}

// Encoders

void inicializa(int num) {
  for(unsigned char i = 0; i < tam; i++) {
    velMedM1[i] = num;
  }
}

float mediaM1(float v) {
  float soma = 0;
  velMedM1[tam - 1] = v;
  for(unsigned char i = 0; i < tam - 1; i++) {
   velMedM1[i] = velMedM1[i + 1];
  }
  for(unsigned char i = 0; i < tam; i++) {
    soma += velMedM1[i];
  }
  return soma / (float) tam;
}

void encoders() {
  leituraEncoder[0][0] = digitalRead(pinEncoders[0]);
  leituraEncoder[0][1] = digitalRead(pinEncoders[1]);
  leituraEncoder[1][0] = digitalRead(pinEncoders[2]);
  leituraEncoder[1][1] = digitalRead(pinEncoders[3]);
}

void encEsqA() {
  if (digitalRead(pinEncoders[1]) != digitalRead(pinEncoders[0])) {
    contEncEsq ++;
  } else {
    contEncEsq --;
  }
  //leituraEncoder[0][0] = digitalRead(pinEncoders[0]);
}

void encDirA() {
  if (digitalRead(pinEncoders[3]) != digitalRead(pinEncoders[2])) {
    contEncDir ++;
  } else {
    contEncDir --;
  }
}

void estrategia(int v) {
  for(int i = 0; i < colunas; i++) {
    if(((int) matEstrategia[colunas][i]) == v) {
      if(((int) matEstrategia[1][i]) == 1) pidVel = reta;
      else pidVel = curva;
      velPID = matEstrategia[0][i];
      return;
    }
  }
}

void setup() {
  Serial.begin(115200);

  reta.setControl(0.13, 0, 2.5, 23.0);
  //reta.setControl(0.08, 0, 1.5, 23.14);   // Inicializar respectivamente as variáveis Kp, Ki, Kd de controle e por último a velocidade
  curva.setControl(0.082, 0, 0.5, 15.0); // tanto para a reta quanto a curva

  pidVel = reta;
  velPID = reta.vel;
  
  // Pinagem da ponte H
  pinMode(pinPWMA, OUTPUT);
  pinMode(pinAIN1, OUTPUT);
  pinMode(pinAIN2, OUTPUT);

  pinMode(pinPWMB, OUTPUT);
  pinMode(pinBIN1, OUTPUT);
  pinMode(pinBIN2, OUTPUT);

  pinMode(pinSTBY, OUTPUT);

  // Sensores frontais e laterais
  for(unsigned char i = 0; i < numSensors; i++) pinMode(portas[i], INPUT);
  pinMode(anaEsq, INPUT);
  pinMode(anaDir, INPUT);

  // Encoders
  for(unsigned char i = 0; i < 4; i++) {
    pinMode(pinEncoders[i], INPUT_PULLUP);
  }
  encoders();
  attachInterrupt(digitalPinToInterrupt(pinEncoders[0]), encEsqA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(pinEncoders[2]), encDirA, CHANGE);

  // Leds e botao
  pinMode(led, OUTPUT);
  pinMode(led, OUTPUT);
  pinMode(bt, INPUT);

  digitalWrite(led, HIGH);

  // Buzzer
  ledcSetup(channel, freq, resolution);
  ledcAttachPin(pinBuzzer, channel);

  // Funções que fazem o papel de setar PWM no esp32
  ledcAttachPin(pinPWMA, 0); // Declara-se o pino e o canal referente ao
  ledcAttachPin(pinPWMB, 1); // PWM

  ledcSetup(0, freqPWM, resolucaoPWM);     // Para cada porta de PWM declarado deve-se setar o canal, a frequência dos pulsos por segundo
  ledcSetup(1, freqPWM, resolucaoPWM);     // e a "resolução" do PWM. O valor de 12 bits corresponde um valor de PWM de 0 a 4095

  while(analogRead(bt) < 2047) delay(100);
  Serial.println("Botao");
  
  Serial.println("Comecou"); // Calibracao
  
  calibrarMedia(50);
  //calibrarMedia(200, 30, 5);
  Serial.println("Terminou");
  
  for(unsigned char i = 0; i < numSensors; i++) {
    Serial.print(i);
    Serial.print("  Min: ");
    Serial.print(maxMin[i][1]);
    Serial.print(", Max: ");
    Serial.print(maxMin[i][0]);
    Serial.print("\n\n\n");
  }
  
  while(analogRead(bt) < 2047) delay(100);
  Serial.println("Botao");
  
  digitalWrite(led, HIGH);

  contEncEsq = 0;
  contEncDir = 0;
  inicializa(0);
  lastTime = micros();

  delay(1000);
  
  ledcWriteTone(channel, 2075);
  ledcWrite(channel, 0);

  inicio = millis();
  timeBuzzer = millis() + 300;
}

void loop() {
  sensorEsquerdo();
  sensorDireito();
  if(millis() - timeBuzzer <= 200) ledcWrite(channel, maxPWM);
  else ledcWrite(channel, 0);
  //if(contDir == 16) terminou = true;
  //boolean condLine = true;
  //for(unsigned char i = 0; i < 5; i++) if(sensors[i] < 250) condLine = false;
  //if(abs(integral) > erroMaximo && condLine) saiuDaLinha = true;
  //if(millis() - inicio > tempoLimite) terminou = true;
  if(saiuDaLinha) while(true) {
    MotorsPWM(0, 0);
    ledcWrite(channel, 0);
    Serial.println("Saiu");
    delay(20);
  } else //Serial.println(readLine(sensors, true) - 2000);
  if(!terminou) {
    segueLinha(pidVel, velPID);
    long dX = contEncEsq - lastcontEncEsq;
    lastcontEncEsq = contEncEsq;
    //Serial.println(contEncEsq);
    velEncEsq = dX*1000000.0/(60.0*(micros() - lastTime));
    lastTime = micros();
    //Serial.println(velEncEsq);
    //Serial.println(mediaM1(velEncEsq));
    Serial.print(contEncEsq);
    Serial.print(" ");
    Serial.print(contEncDir);
    Serial.print("\t\n");
  } else {
    unsigned long t = millis();
    while(millis() < t + 300) {
         segueLinha(reta, 20.0);
         delay(1);
         digitalWrite(led, HIGH);
    }
    contDir = 10000;
    ledcWrite(channel, 0);
    while(true) MotorsPWM(0, 0);
  }
  delay(1);
}
