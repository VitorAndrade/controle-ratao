/* 05/09/2019
 * Leitura com dupla precisão
 */

#include <SimpleKalmanFilter.h>

SimpleKalmanFilter leftEncKalman (0.5, 0.5, 0.01);
SimpleKalmanFilter rightEncKalman(0.5, 0.5, 0.01);

// Encoders
const unsigned char pinEncoders[4] = {15, 21, 23, 22}; // As duas primeiras portas corresponde as portas A e B do primeiro encoder enquanto
volatile long contEncEsq = 0;                          // as as duas portas seguintes corresponde as portas A e B do segundo encoder
volatile long contEncDir = 0;
long lastContEncEsq = 0;
unsigned long lastTimeEsq = 0;
unsigned long lastTimeDir = 0;
float velEncEsq = 0;
long lastContEncDir = 0;
float velEncDir = 0;

void encEsqA() {
  if (digitalRead(pinEncoders[1]) != digitalRead(pinEncoders[0])) {
    contEncEsq ++;
  } else {
    contEncEsq --;
  }
}

void encEsqB() {
  if (digitalRead(pinEncoders[1]) == digitalRead(pinEncoders[0])) {
    contEncEsq ++;
  } else {
    contEncEsq --;
  }
}

void encDirA() {
  if (digitalRead(pinEncoders[3]) != digitalRead(pinEncoders[2])) {
    contEncDir ++;
  } else {
    contEncDir --;
  }
}

void encDirB() {
  if (digitalRead(pinEncoders[3]) == digitalRead(pinEncoders[2])) {
    contEncDir ++;
  } else {
    contEncDir --;
  }
}

void updateLeftEncoder() {
  long dX = contEncEsq - lastContEncEsq;
  long dT = micros() - lastTimeEsq;
  if(dX == 0) {
    if(dT >= 20000) velEncEsq = leftEncKalman.updateEstimate(0);
    return;
  }
  lastContEncEsq = contEncEsq;
  velEncEsq = dX*1000000.0/(120.0*dT);
  velEncEsq = leftEncKalman.updateEstimate(velEncEsq);
  lastTimeEsq = micros();
}

void updateRightEncoder() {
  long dX = contEncDir - lastContEncDir;
  long dT = micros() - lastTimeDir;
  if(dX == 0) {
    if(dT >= 20000) velEncDir = rightEncKalman.updateEstimate(0);
    return;
  }
  lastContEncDir = contEncDir;
  velEncDir = dX*1000000.0/(120.0*dT);
  velEncDir = rightEncKalman.updateEstimate(velEncDir);
  lastTimeDir = micros();
}

void updateEncoders() {
  updateLeftEncoder();
  updateRightEncoder();
}

void setup() {
  Serial.begin(115200);
  for(unsigned char i = 0; i < 4; i++) {
    pinMode(pinEncoders[i], INPUT_PULLUP);
  }
  attachInterrupt(digitalPinToInterrupt(pinEncoders[0]), encEsqA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(pinEncoders[1]), encEsqB, CHANGE);
  attachInterrupt(digitalPinToInterrupt(pinEncoders[2]), encDirA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(pinEncoders[3]), encDirB, CHANGE);
}

void loop() {
  updateEncoders();
  Serial.print(velEncEsq);
  Serial.print(" ");
  Serial.println(velEncDir);
}
