#define tam 25

unsigned char pinEncoders[4] = {15, 21, 23, 22}; //{13, 21, 15, 7};
volatile bool leituraEncoder[2][2]; // primeiro índice: 0 - encoder esquerdo e 1 - encoder direito
                                    // segundo índice: 0 - porta A e 1 - porta B
long contEncEsq = 0;
long contEncDir = 0;
long lastcontEncEsq = 0;
unsigned long lastTime = 0;
float velEncEsq = 0;

int velMedM1[tam];

void inicializa(int num) {
  for(unsigned char i = 0; i < tam; i++) {
    velMedM1[i] = num;
  }
}

float mediaM1(float v) {
  float soma = 0;
  velMedM1[tam - 1] = v;
  for(unsigned char i = 0; i < tam - 1; i++) {
   velMedM1[i] = velMedM1[i + 1];
  }
  for(unsigned char i = 0; i < tam; i++) {
    soma += velMedM1[i];
  }
  return soma / (float) tam;
}

void encoders() {
  leituraEncoder[0][0] = digitalRead(pinEncoders[0]);
  leituraEncoder[0][1] = digitalRead(pinEncoders[1]);
  leituraEncoder[1][0] = digitalRead(pinEncoders[2]);
  leituraEncoder[1][1] = digitalRead(pinEncoders[3]);
}

void encEsqA() {
  if (digitalRead(pinEncoders[1]) != digitalRead(pinEncoders[0])) {
    contEncEsq ++;
  } else {
    contEncEsq --;
  }
  //leituraEncoder[0][0] = digitalRead(pinEncoders[0]);
}

void encDirA() {
  if (digitalRead(pinEncoders[3]) != digitalRead(pinEncoders[2])) {
    contEncDir ++;
  } else {
    contEncDir --;
  }
}

//Define the Motors Pins

//Motor 1
int pinAIN1 = 16; //15; //5; // Direction
int pinAIN2 = 4; //8;  //4; // Direction
int pinPWMA = 2; //0 esp32 antigo; //32; //3; // Speed

//Motor 2
int pinBIN1 = 5;  //0;  //7; // Direction
int pinBIN2 = 18; //4;  //8; // Direction
int pinPWMB = 19; //25; //9; // Speed

//Standby
int pinSTBY = 17; //2; //6;

//Constants to help remember the parameters of the motors
bool turnCW = false;
bool turnCCW = true;
bool motor1 = false;
bool motor2 = true;

//Sensors right and left
bool lastEsq = false;
bool flagEsq = false;
bool lastDir = false;
bool flagDir = false;

void MotorsPWM(int m2, int m1) {   // Função que recebe os valores PWM para ambos motores, direito e esquerdo
  bool sentido1;                   // O valor negativo inverte o sentido de rotação do motor
  bool sentido2;
  m1 = -m1;                        // Pela forma que o robô foi montado, se mandarmos o mesmo PWM para ambos motores eles girarão em sentidos opostos, por isso deve-se inverter o sentido de um motor multiplicando por -1
  if(m1 < 0) {
    sentido1 = HIGH;
    m1 = -m1;
  } else sentido1 = LOW;
  
  if(m2 < 0) {
    sentido2 = HIGH;
    m2 = -m2;
  } else sentido2 = LOW;

  if(m1 > 255) m1 = 255;
  if(m2 > 255) m2 = 255;
  
  // Motor 1
  digitalWrite(pinAIN1, sentido1);
  digitalWrite(pinAIN2, !sentido1);  //This is the opposite of the AIN1
  //analogWrite(pinPWMA, m1);  // PWM do arduino
  ledcWrite(0, m1);            // PWM do esp32
  
  // Motor 2
  digitalWrite(pinBIN1, sentido2);
  digitalWrite(pinBIN2, !sentido2);  //This is the opposite of the BIN1
  //analogWrite(pinPWMB, m2);  // PWM do arduino
  ledcWrite(1, m2);            // PWM do esp32
   
  // Finally , make sure STBY is disabled - pull it HIGH
  digitalWrite(pinSTBY, HIGH);
}
                  
void setup() {
  Serial.begin(115200);
  
  // Pinagem da ponte H
  pinMode(pinPWMA, OUTPUT);
  pinMode(pinAIN1, OUTPUT);
  pinMode(pinAIN2, OUTPUT);

  pinMode(pinPWMB, OUTPUT);
  pinMode(pinBIN1, OUTPUT);
  pinMode(pinBIN2, OUTPUT);

  pinMode(pinSTBY, OUTPUT);

  // Funções que fazem o papel de setar PWM no esp32
  ledcAttachPin(pinPWMA, 0); // Declara-se o pino e o canal referente a
  ledcAttachPin(pinPWMB, 1); // esse pino

  ledcSetup(0, 5000, 8);     // Para cada canal declarado deve-se setar o canal, a frequência dos pulsos por segundo
  ledcSetup(1, 5000, 8);     // e a "resolução" do PWM. O valor de 8 bits corresponde um valor de PWM de 0 a 255
  for(unsigned char i = 0; i < 4; i++) {
    pinMode(pinEncoders[i], INPUT_PULLUP);
  }
  encoders();
  attachInterrupt(digitalPinToInterrupt(pinEncoders[0]), encEsqA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(pinEncoders[2]), encDirA, CHANGE);
  contEncEsq = 0;
  contEncDir = 0;
  inicializa(0);
  lastTime = micros();
}

void loop() {
  //encoders();
  long dX = contEncEsq - lastcontEncEsq;
  lastcontEncEsq = contEncEsq;
  //Serial.println(contEncEsq);
  velEncEsq = dX*1000000.0/(60.0*(micros() - lastTime));
  lastTime = micros();
  for(int i = -255; i < 256; i++) {
    MotorsPWM(i, i);
    delay(15);
  }
  for(int i = 255; i >= -255; i--) {
    MotorsPWM(i, i);
    delay(15);
  }
  Serial.print(contEncEsq);
  Serial.print(" ");
  Serial.print(contEncDir);
  Serial.print("\t\n");
  delay(20);
  /*
  Serial.print("A: ");
  Serial.print(leituraEncoder[0][0]);
  Serial.print("  B: ");
  Serial.println(leituraEncoder[0][1]);
  */
}
