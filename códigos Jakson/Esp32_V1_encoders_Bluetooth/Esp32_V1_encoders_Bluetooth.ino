#include "BluetoothSerial.h"
#define tam 25

BluetoothSerial SerialBT;

unsigned char pinEncoders[4] = {15, 21, 23, 22}; //{13, 21, 15, 7};
volatile bool leituraEncoder[2][2]; // primeiro índice: 0 - encoder esquerdo e 1 - encoder direito
                                    // segundo índice: 0 - porta A e 1 - porta B
long contEncEsq = 0;
long contEncDir = 0;
long lastcontEncEsq = 0;
unsigned long lastTime = 0;
float velEncEsq = 0;

int velMedM1[tam];

void inicializa(int num) {
  for(unsigned char i = 0; i < tam; i++) {
    velMedM1[i] = num;
  }
}

float mediaM1(float v) {
  float soma = 0;
  velMedM1[tam - 1] = v;
  for(unsigned char i = 0; i < tam - 1; i++) {
   velMedM1[i] = velMedM1[i + 1];
  }
  for(unsigned char i = 0; i < tam; i++) {
    soma += velMedM1[i];
  }
  return soma / (float) tam;
}

void encoders() {
  leituraEncoder[0][0] = digitalRead(pinEncoders[0]);
  leituraEncoder[0][1] = digitalRead(pinEncoders[1]);
  leituraEncoder[1][0] = digitalRead(pinEncoders[2]);
  leituraEncoder[1][1] = digitalRead(pinEncoders[3]);
}

void encEsqA() {
  if (digitalRead(pinEncoders[1]) != digitalRead(pinEncoders[0])) {
    contEncEsq ++;
  } else {
    contEncEsq --;
  }
  //leituraEncoder[0][0] = digitalRead(pinEncoders[0]);
}

void encDirA() {
  if (digitalRead(pinEncoders[3]) != digitalRead(pinEncoders[2])) {
    contEncDir ++;
  } else {
    contEncDir --;
  }
}
                  
void setup() {
  for(unsigned char i = 0; i < 4; i++) {
    pinMode(pinEncoders[i], INPUT_PULLUP);
  }
  encoders();
  attachInterrupt(digitalPinToInterrupt(pinEncoders[0]), encEsqA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(pinEncoders[2]), encDirA, CHANGE);
  SerialBT.begin("Ratao 3.0");
  contEncEsq = 0;
  contEncDir = 0;
  inicializa(0);
  lastTime = micros();
}

void loop() {
  //encoders();
  long dX = contEncEsq - lastcontEncEsq;
  lastcontEncEsq = contEncEsq;
  //Serial.println(contEncEsq);
  velEncEsq = dX*1000000.0/(60.0*(micros() - lastTime));
  lastTime = micros();
  //Serial.println(velEncEsq);
  //Serial.println(mediaM1(velEncEsq));
  SerialBT.print(contEncEsq);
  SerialBT.print(" ");
  SerialBT.print(contEncDir);
  SerialBT.print("\t\n");
  delay(20);
  /*
  Serial.print("A: ");
  Serial.print(leituraEncoder[0][0]);
  Serial.print("  B: ");
  Serial.println(leituraEncoder[0][1]);
  */
}
