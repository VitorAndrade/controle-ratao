/*  02/09/2019
    Melhoria nas funções de encoder
    Código comentado
    Menu para troca de velocidades com som
    Adaptado para a pista do linusbot
    Melhora na leitura dos sensores laterais
    Utiliza o filtro de Kalman
    Para quando sai no branco e no preto
    Matriz estratégia: n linhas por 3 colunas
    Identifica se saiu da linha
    Com sensores laterais com calibração
    Pista ao contrário
    Controle PID novo e funcional
    Velocidade em percentual
*/

// Condições de parada
bool terminou = false;
unsigned int tempoLimite = 500000;           // Tempo em milissegundos após o momento em que o robô passa a seguir a linha

// Para alternar entre pista com linha preta ou com linha branca
boolean whiteLine = true;                    // Por padrão se utiliza true; para a pista do linusbot se utiliza false

// Filtro de Kalman
#include <SimpleKalmanFilter.h>              // O filtro de Kalman tem o papel de filtrar a leitura dos sensores, atenuando o erro associado ao mesmo
// através da estimativa do valor mais próximo ao real

SimpleKalmanFilter simpleKalmanFilter(100, 2, 4.00);
SimpleKalmanFilter leftEncKalman(0.5, 0.5, 0.1);
SimpleKalmanFilter rightEncKalman(1000, 2, 10.00);

boolean kalman = false;    //True com filtro, false sem filtro.

/* * * * * * * * * * * * * * * * * * * * * * * Estratégia * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   A estratégia consiste na lógica de selecionar o controle PID e velocidade mais adequados para determinados trechos
   da pista. Ela se dá por uma matrix formada por 3 colunas e n linhas, onde a primeira linha se refere a 'velocidade
   linear' do robô em percentual, a segunda linha se refere ao tipo de PID que se deseja utilizar (reta, curva, etc.)
   e a terceira coluna diz respeito quando se deve mudar o controle PID e/ou a velocidade do robô. Para esse último,
   0 significa a configuração inicial do robô, valores negativos representam a leitura do contador direito e valores
   positivos em contrapartida representam a a leitura do contador esquerdo.
*/

short velPID = 0;     // Velociade linear atual em percentual
const int linhas = 4; // Número de linhas da matriz utilizada, quando alterar a matriz deve-se verificar a necessidade de se modificar o número de linhas

float matEstrategia [linhas][3] = {
  {50.0, 3,   0},
  {23.0, 1,   2},
  {25.0, 4,   3},
  {28.0, 5,   4}
};

//float matEstrategia [linhas][3] = {
//                    {23.0, 1,   0}};

/*
  float matEstrategia [linhas][3] = {
                    {23.0, 1,   0},
                    {18.0, 2,  -1},
                    {23.0, 1,  -2},
                    {50.0, 3, -13},
                    {8.00, 1, -17},
                    {23.0, 1, -18}};
*/

/*
  float matEstrategia [linhas][3] = {
                    {23.0, 1,  0},
                    {20.0, 2,  1},
                    {23.0, 2,  3},
                    {24.0, 2,  3},
                    {70.0, 2, -2},
                    {18.0, 2, -3},
                    {23.0, 1, 16},
                    {70.0, 1, -4},
                    {18.0, 1, -5},
                    {20.0, 2, 19},
                    {80.0, 1, -6},
                    {20.0, 2, -7},
                    {80.0, 1, -8},
                    {0.00, 1, -9},
                    {23.0, 1, -10},
                    //{23.0, 1, 33},
                    //{18.0, 1, 36},
                    {70.0, 1, -11},
                    {18.0, 1, -13},
                    {23.0, 1, -14},
                    {24.0, 1, -15}};
                       v , t, c  -> v: velocidade linear, valor real entre 0 e 100.0 onde 100.0 corresponde a potência máxima;
                                 -> t: tipo de PID. Tipo 1: reta, tipo 2: curva;
                                 -> c: contador direito ou esquerdo. Se positivo corresponde ao esquerdo, caso contrário corresponde
                                       ao contador direito;
                                 OBS.: ao modificar a matriz deve-se também alterar o número de linhas caso necessário. */


// Menu
int selecionaPID = 0; // Para selecionar o tipo de PID quando se utilizar o menu pelo botão

/* * * * * * * * * * * * * * * * * * * * Sair da linha * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Nos códigos mais antigos em que não se havia nenhum critério de parada do robô após ele ter saído da linha, even-
   tualmente acontecia dele estragar após ter saído da linha em alta velocidade e colidido. Pensando nesse problema,
   foi desenvolvido uma lógica para o robô parar de se movimentar caso ele identificar que não está mais seguindo li-
   nha. A lógica consiste em analisar a leitura a analógica dos sensores frontais e medir o contraste da leitura de-
   les, ou melhor, um algoritmo que calcula a diferença entre a maior e menor leitura dos sensores frontais. Quando
   o robô está seguindo linha, na maior parte do tempo alguns dos sensores estão sobre a linha branca enquanto os ou-
   tros sensores estão identificando a cor preto da pista, logo o contraste será grande nesse caso e pequeno quando o
   robô estiver totalmente fora da linha idependentimente da cor piso exterior á pista (considerando um cor qualquer
   uniformemente distribuída).
*/

boolean saiuDaLinha = false;   // Se o robô saiu totalmente da linha
unsigned int erroMaximo = 200; // Somatório máximo do erro até identificar que o robô saiu da linha
unsigned long somaErros = 0;   // Somatório dos erros
unsigned long tempoErro = 0;   // Registro do tempo em que o robô ficou pela última vez totalmente fora da linha

/* * * * * * * * * * * * * * * * * * * * * * * * Buzzer * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Consiste em um sinal sonoro para múltiplas funcionalidades: para identificar quando o botão é pressionado para en-
   trar no menu, para selecionar alguma opção no menu, para quando o robô começa a seguir a linha e também atualmente
   é usado para indicar quando o robô mudou de estratégia.
*/

int freq = 2000;               // Frequência
int channel = 3;               // Canal do PWM
int resolution = 8;            // Resolução em bits
unsigned char pinBuzzer = 14;  // Pino digital do esp32
unsigned long timeBuzzer = 0;  // Tempo onde o buzzer tocou pela última vez

// Encoders
const unsigned char pinEncoders[4] = {15, 21, 23, 22}; // As duas primeiras portas corresponde as portas A e B do primeiro encoder enquanto
volatile bool leituraEncoder[2][2];                    // as as duas portas seguintes corresponde as portas A e B do segundo encoder
long contEncEsq = 0;
long contEncDir = 0;
long lastcontEncEsq = 0;
unsigned long lastTimeEsq = 0;
float velEncEsq = 0;
long lastcontEncDir = 0;
unsigned long lastTimeDir = 0;
float velEncDir = 0;

// Leds e botao
const unsigned char ledVermelho = 14; // Porta digital do esp32
const unsigned char ledVerde = 27;
const unsigned char bt = 13;

// Controle PID
const unsigned char  resolucaoPWM = 12;                      // Permite valores de PWM de 0 a 4095
const unsigned int   freqPWM      = 20000;                   // Número de em que o esp32 produz o pulso de PWM por segundo
const int            maxPWM       = 4095;                    // Maior valor do PWM
const float          ajuste       = 100.0 * maxPWM / 255.0;  // Esse cálculo é necessário para quando se alterar a resolução de PWM
// ainda se manter os mesmos valores para o controle PID

/* * * * * * * * * * * * * * * * * * * * * * * * * * * Controle PID * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   De todos os algoritmos usados para o pleno funcionamento do robô seguidor de linha, a lógica de PID é um dos mais importantes pois permite
   controlar seu movimento e assim seguir a linha. Mas o que vem a ser o controle PID? Consiste de um sistema de controle onde há três pesos
   (proporional, integral e derivativo) com a função de de minimizar um erro, fazer com que ele fique mais próximo de 0. No seguidor usamos a
   função readLine() para identificar o quão afastado (para a direita ou esquerda) estão os sensores frontais da linha e apartir do valor que
   ela retorna calculamos o erro. Para que o controle funcione, é preciso identificar os melhores pesos para os ganhos de PID. Atualmente te-
   mos o ganho integrativo igual a zero devido a dificuldade em encontrar um bom valor, um vez que eles estão sendo encontrados por tentati-
   va e erro.
*/

struct Control { // Estrutura para salvar os controles de PID e velocidade
  float Kp = 0;  // Constante para o ganho proporcional
  float Ki = 0;  // Constante para o ganho integrativo
  float Kd = 0;  // Constante para o ganho derivativo
  float vel = 0; // Velocidade linear

  void setControl(float p, float i, float d, float v) { // Função para setar os parâmetros de PID e velocidade
    Kp = p;
    Ki = i;
    Kd = d;
    vel = v;
  }

  void setControl(float p, float i, float d) {        // Função para setar os parâmetros de PID com velocidade linear igual a 0 (zero)
    Kp = p;
    Ki = i;
    Kd = d;
    vel = 0;
  }
};

// Declaração para diferentes tipos de controle

Control reta;  // Controle para reta
Control curva; // Controle para curva
Control curvaFechada;
Control retaRapida;
Control PID5;
Control pidVel;

unsigned long inicio = 0; // Salva o tempo em milissegundos do momento em que o esp32 foi ligado ao momento em que o robô passa a seguir a linha

bool condicao = false;   // "condição" é uma variável utilizada para se alternar o controle entre reta e curva
bool cruzamento = false; // "cruzamento" é uma variável utilizada para verificar se há cruzamento (cruzamento == true) ou não (cruzamento == false)

int last_proportional = 0; // Variável que armazena o último erro (proportional), o qual varia entre -2000 e 2000
long integral = 0;         // Somatório de todos erros (proportional)

/* * * * * * * * * * * * * * * * * * * * * * * * * * Sensores Laterais * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   A pista usada nas competições de seguidor de linha no Brasil no geral seguem as regras da Robocore onde, entre outras regras, deve
   possuir marcações laterais à linha para identificar o início, término, as curvas, cruzamento e retas. Para isso, o ratão detém um par
   de sensores laterais (esquerdo e direito). A partir da leitura desses sensores o robô consegue se localizar onde ele está, possibili-
   tando criar estratégias levando em consideração a velocidade linear e o tipo de controle para diferentes trechos.
   Os sensores laterais como os frontais possuem calibração para permitir o pleno funcionamento da leitura das faixas laterais indepen-
   dente da iluminação local.
*/

// Pinos laterais analógicos
const unsigned char anaEsq = 26; // Pino sensor lateral esquerdo
const unsigned char anaDir = 36; // Pino sensor lateral direito

unsigned short avgEsq = 500;  // Caso os sensores esquerdo ou direito estejam abaixo de um determinado valor é porque o robô
unsigned short avgDir = 500;  // identificou uma marcação lateral

// lastEsq e lastDir armazenam a leitura de seus respectivos sensores laterais
// flagEsq e flagDir são usadas na lógica de identificação das marcações laterais
bool lastEsq = false;
bool flagEsq = false;
bool lastDir = false;
bool flagDir = false;

int contEsq = 0;  // Os contadores armazenam o número de vezes em que foi identifcado as marcações
int contDir = 0;  // dos sensores laterais esquerdo ou direito

/* * * * * * * * * * * * * * * * * * * * * * * * * Sensores frontais e calibracao * * * * * * * * * * * * * * * * * * * * * * * *
   Como queremos que o nosso robô siga a linha é no mínimo esperado que ele seja capaz de identificá-la. Mas como o Ratão faz essa
   proeza? Simples, igual a muitos outros robôs o nosso possui 5 sensores infravermelhos frontais e através da leitura deles é cal-
   culado o quão distante ele esta na linha.
   Na nossa categoria a iluminação ambiente desempenha um papel determinante na identificação da linha, pois em excesso ela ofusca
   os sensores receptores impossibilitando que eles distinguam as cores preto e branco da pista. Todavia, seu oposto não é prejudi-
   cial. Devido a presença de emissores infravermelhos o Ratão consegue localizar a linha até mesmo no escuro (sim, já colocamos e-
   le para seguir a linha na escuridão total).
   Para contornar os problemas de iluminação, adotamos a calibração de cada sensor receptor infravermelho. Durante a calibração, ca-
   da sensor é registrado os valores de maior e menor leitura (valores entre 0 e 4095) e depois fazemos o mapeamento da leitura atu-
   al de 0 a 1000.
*/

const unsigned char numSensors = 5;               // Quantidade de sensores frontais
const unsigned char numLateralSensors = 2;        // Quantidade de senores laterais
unsigned int maxMin[numSensors][2];  // 0 -> Max  // Essa matrix salva os valores de leitura de máximo e mínimo dos sensores frontais
// 1 -> Min
unsigned int maxMinLateral[numLateralSensors][2] = {{0, 0}, {1000, 1000}};  // 0 -> Max // Matrix análoga a de cima
// 1 -> Min

unsigned char portas[numSensors] = {33, 32, 35, 34, 39}; //{39, 34, 35, 32, 33}; // Vetor que armazena as portas dos sensores frontais (a ordem é importante)
unsigned int sensors[numSensors];               // Vetor usado para salvar a leitura dos sensores frontais depois de calibrados para a lógica de controle
unsigned int lateralSensors[numLateralSensors]; // Vetor usado para salvar a leitura dos sensores laterais depois de calibrados para a identificação das marcações laterais
unsigned int amplitude = 1000;                  // A leitura depois de calibrada se limita ao intervalo entre 0 e amplitude (valor inteiro positivo)
boolean calibrado = false;                      // Se foi feita ou não a calibração

// Funções sensores laterais

void retaCurva(int c) {    // Função usada para alternar o valor da condição entre true e false, a partir do valor do contador esquerdo (contEsq)
  condicao = true;
  if (c % 2 == 0) condicao = false;
}

void detectaCruzamento() { // Esta função é chamada quando um dos sensores laterais identifica uma marcação lateral, com o objetivo do robô distingua
  if (analogRead(anaEsq) < avgEsq && analogRead(anaDir) < avgDir) { // se é realmente uma marcação ou cruzamento
    cruzamento = true;
  } else {
    cruzamento = false;
  }
}

void sensorEsquerdo() { // Função que verifica se sensor esquerdo passou sobre a faixa lateral esquerda, caso ocorra contesq++
  lastEsq = flagEsq;
  int leituraEsquerda = (int) map(analogRead(anaEsq), maxMinLateral[0][1], maxMinLateral[0][0], 0, 1000); // A leitura digital retorna 0 para branco e 1 para preto, A2 é a porta analógica do arduino onde se encontra o sensor lateral esquerdo
  //Serial.print(leituraEsquerda);
  //Serial.print("  ");
  //Serial.print(analogRead(anaEsq)); Serial.print("  ");
  if (leituraEsquerda > 200 && leituraEsquerda < 800) return;
  if (leituraEsquerda < avgEsq) flagEsq = true;    // Se a leitura do sensor lateral for igual a zero significa que o robô leu a faixa a lateral, logo a flagEsq recebe true
  else flagEsq = false;                            // caso contrário recebe false

  if (flagEsq && !lastEsq) {                       // Se o robô leu a faixa lateral branca mas anteriormente não o tinha lido, tem de ser adicionado mais um ao contador esquerdo.
    //tempoEsq = millis();
    //detectaCruzamento();
    if (!cruzamento) {
      contEsq++;
      estrategia(contEsq);
      //timeBuzzer = millis();
      //retaCurva(contEsq);                   // Função usada para alteranar a velocidade entre reta e curva
      Serial.print("Esquerdo: ");
      Serial.println(contEsq);
    }
    else Serial.println("Cruzamento");
  }

  if (!flagEsq && lastEsq) {

  }
}

void sensorDireito() { // Funcão análoga a função sensorEsquerdo()
  lastDir = flagDir;
  int leituraDireita = (int) map(analogRead(anaDir), maxMinLateral[1][1], maxMinLateral[1][0], 0, 1000);

  //Serial.print("  ");
  //Serial.print(leituraDireita);
  //Serial.println("\t");

  if (leituraDireita > 200 && leituraDireita < 800) return;

  if (leituraDireita < avgDir) flagDir = true;
  else flagDir = false;

  if (flagDir && !lastDir)  {
    //detectaCruzamento();
    if (!cruzamento) {
      contDir++;
      estrategia(-contDir);
      //timeBuzzer = millis();
      Serial.print("Direito: ") ;
      Serial.println(contDir);
    } else Serial.println("Cruzamento");
  }

  if (!flagDir && lastDir) {

  }
}

int mapeamento(int x, int in_min, int in_max, int out_min, int out_max) {                       // Função semelhante a função map(). Ela tem o papel de mapear um determinado valor
  if (in_max != in_min) return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min; // de um certo intervalo para outro
  else return out_min;
}

float mapFloat(float x, float in_min, float in_max, float out_min, float out_max) {               // Função semelhante a função mapeamento, com a diferença no uso de valores do tipo
  if (in_max != in_min) return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;  // float
  else return out_min;
}

void inicializaMaxMin(unsigned int max = 0, unsigned int min = 90000) { // Para inicializar o vetor maxMin uma vez que em C++ um vetor quando não inicializado
  for (unsigned char i = 0; i < numSensors; i++) {                      // armazena valores aletórios
    maxMin[i][0] = max;
    maxMin[i][1] = min;
  }
}

void calibrar() { // Para cada sensor deve salvar de sua leitura os valores máximos e mínimos
  for (unsigned char i = 0; i < numSensors; i++) {
    if (analogRead(portas[i]) > maxMin[i][0]) maxMin[i][0] = analogRead(portas[i]); // 0 -> Max (maior leitura lida)
    if (analogRead(portas[i]) < maxMin[i][1]) maxMin[i][1] = analogRead(portas[i]); // 1 -> Min (menor leitura lida)
  }
  calibrado = true;
}

void calibrar(unsigned int numVezes, unsigned int tempo = 10) { // Calibrar na maneira mais simples. Ela não é muito recomendada pois os sensores as vezes fazem uma leitura
  inicializaMaxMin();                                           // com valores muito distantes da realidade
  for (int i = 0; i < numVezes; i++) {
    calibrar();
    delay(tempo);
  }
  calibrado = true;
}

void calibrarMedia(int *somatorio, unsigned int tempo, unsigned char num) { // Para cada sensor deve salvar de sua leitura os valores máximos e mínimos. Essa função
  int value;                                                                // faz a calibração através dos valores de leitura médios
  for (unsigned char i = 0; i < numSensors; i++) somatorio[i] = 0; // Inicializa o vetor com 0
  for (unsigned char i = 0; i < num; i++) {                       // Soma "num vezes" as leituras dos sensores para cada índice do vetor em um certo espaço de tempo
    for (unsigned char i = 0; i < numSensors; i++) somatorio[i] += analogRead(portas[i]);
    delay(tempo);
  }
  for (unsigned char i = 0; i < numSensors; i++) { // Aqui é calculado o máximo ou mínimo médio para cada sensor individual
    value = somatorio[i] / num;
    if (value > maxMin[i][0]) maxMin[i][0] = value; // 0 -> Max
    if (value < maxMin[i][1]) maxMin[i][1] = value; // 1 -> Min
  }
  calibrado = true;
}

void calibrarMedia(int *vet1, int *vet2, unsigned int tempo, unsigned char num) { // Para cada sensor deve salvar de sua leitura os valores máximos e mínimos. Essa função
  int value;                                                                      // faz a calibração dos sensores frontais e laterais de forma simultânea
  for (unsigned char i = 0; i < numSensors; i++)        vet1[i] = 0; // Inicializar os vetores vet1 e vet2 com 0
  for (unsigned char i = 0; i < numLateralSensors; i++) vet2[i] = 0;
  for (unsigned char i = 0; i < num; i++) {                        // Soma num vezes as leituras dos sensores
    for (unsigned char i = 0; i < numSensors; i++) vet1[i] += analogRead(portas[i]);
    vet2[0] += analogRead(anaEsq);
    vet2[1] += analogRead(anaDir);
    delay(tempo);
  }
  for (unsigned char i = 0; i < numSensors; i++) {
    value = vet1[i] / num;
    if (value > maxMin[i][0]) maxMin[i][0] = value; // 0 -> Max
    if (value < maxMin[i][1]) maxMin[i][1] = value; // 1 -> Min
  }
  for (unsigned char i = 0; i < numLateralSensors; i++) {
    value = vet2[i] / num;
    if (value > maxMinLateral[i][0]) maxMinLateral[i][0] = value; // 0 -> Max
    if (value < maxMinLateral[i][1]) maxMinLateral[i][1] = value; // 1 -> Min
  }
  calibrado = true;
}

// CALIBRAÇÃO: A função a baixo é a principal função para calibra os sensores. Ela recebe 3 parâmetros que pode ser livremente escolhidos,
//             porém é recomendado alterar apenas o "número de vezes" em que a função vai ficar calibrando onde cada ciclo leva 100 milis-
//             segundos

void calibrarMedia(unsigned int vezes = 5, unsigned int tempo = 10, unsigned char num = 10) {
  int somatorio[numSensors];
  int vet2[numLateralSensors];
  inicializaMaxMin();
  for (unsigned char i = 0; i < vezes; i++) calibrarMedia(somatorio, vet2, tempo, num);
}

// Funções para o cálculo de erro e controle PID

void read_sensors(unsigned int *_sensors) { // Função para salvar a leitura dos sensores em um vetor frontais uma vez que eles foram devidamente calibrados
  for (unsigned char i = 0; i < numSensors; i++) {
    int leitura = analogRead(portas[i]);
    if (calibrado) {                        // Caso já esteja calibrado, se a leitura atual do sensor de porta[i] estiver no intervalo entre seu de menor e maior valor correspondente,
      if (leitura > maxMin[i][1] && leitura < maxMin[i][0]) _sensors[i] = mapeamento(leitura, maxMin[i][1], maxMin[i][0], 0, amplitude); // deve-se mapear o valor no
      else {                                // intervalo entre 0 e a amplitude
        if (leitura < maxMin[i][1]) _sensors[i] = 0; // Caso contrário deve-se salvar 0 mínimo se a leitura for menor que o valor mínimo
        else if (leitura > maxMin[i][0]) _sensors[i] = amplitude;              // ou salvar o valor da amplitude caso a leitura tenha valor maior
      }
    } else _sensors[i] = mapeamento(leitura, 0, 4095, 0, amplitude);
  }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * Cálculo do erro * * * * * * * * * * * * * * * * * * * * * * * *
 * Como o Ratão sabe o quanto está afastado da linha, se está alinhado, fora ou em uma posição intermediária? Para o
 * controle PID é fundamental termos essa informação já que ela é usada no cálculo do erro.
 * A função abaixo retorna a posição do robô em relação a linha quando passado como parâmetro um vetor (onde são ar-
 * mazenadas as leituras individuais dos sensores frontais) e um booleano para selecionar o modo (quando true o robô
 * deve seguir alguma linha branca em uma pista com fundo preto. O valor retornardo é um número inteiro que vai de 0
 * a 4000 para 5 sensores. O valor máximo atingido é igual a: (numSensors - 1) * 1000. Quando a função retorna 0 ou
 * 4000 é porque o robô saiu totalmente pelo lado esquerdo ou direito. Por outro lado, quando o valor retornado é i-
 * gual a metade do valor máximo significa que o robô está alinhado a linha ou é um cruzameto.
 */

int readLine(unsigned int *_sensors, boolean mode = false) {
  unsigned long avr = 0;
  unsigned int soma = 0, max = 0, min = amplitude;
  boolean on_line = false;
  static int last_value = 0;
  int value[numSensors];
  read_sensors(_sensors);
  for (unsigned char i = 0; i < numSensors; i++) {
    value[i] = _sensors[i];
    if (mode)  value[i] = amplitude - value[i];
    if (value[i] > max) max = value[i];
    if (value[i] < min) min = value[i];
    if (value[i] > amplitude * 0.8) on_line = true;
  }
  if (!on_line) {
    if (!kalman) {
      if (last_value < (numSensors - 1) * 500) return 0;
      else return (numSensors - 1) * 1000;
    } else {
      if (last_value < (numSensors - 1) * 500) return (int) simpleKalmanFilter.updateEstimate(0);
      else return (int) simpleKalmanFilter.updateEstimate((numSensors - 1) * 1000);
    }
  } else {
    for (unsigned char i = 0; i < numSensors; i++) {
      //value[i] = mapeamento(value[i], min, max, 0, amplitude);
      avr  += 1000 * i * value[i];
      soma += value[i];
    }
    if (kalman)last_value = (int) simpleKalmanFilter.updateEstimate(avr / soma);
    else last_value = avr / soma;
  }
  return last_value;
}

/* * * * * * * * * * * * * * * * * * * * * * * * Funções de contre PID * * * * * * * * * * * * * * * * * *
 * Temos 3 funções para fazer o controle PID onde a primeira recebe quatro parâmetros, a segunda recebe 2
 * e terceira recebe 1 parâmetro. Todas funcionam de forma quase idêntica. O que distinguem em cada uma de-
 * las é o seu uso. 
 */

void segueLinha(float p, float i, float d, float vel) {        // Função que faz o controle do robô, a qual recebe os parâmetros de PId e velocidade
  unsigned int position = readLine(sensors, whiteLine);        // A variável position recebe o valor da posição do robô em relação a linha, variando entre 0 e 4000. Pela configuração atual o robô faz a leitura da sua posição em relação a uma linha branca em uma pista preta, caso queira que o robô leia uma linha preta sobre uma pista branca => unsigned int position = qtra.readLine(sensors);
  int proportional = position - 2000;                          // A variável proportional armazena o erro que vai de -2000 a 2000, uma vez que position varia entre 0 e 4000
  int derivative = proportional - last_proportional;           // A variável derivative recebe a variação do erro

  if ((proportional <= 0 && last_proportional >= 0) || (proportional >= 0 && last_proportional <= 0) || abs(proportional) == 2000) { // Se o robô estiver sobre a linha (proportional == 0) ou se ele passar sobre ela, a variável integral que é um somatório de todos os erros
    if (millis() - tempoErro > 3000) {
      integral = 0;
      tempoErro = millis();
      somaErros = 0;
    }                                                                                                                   // deve receber 0
  }

  integral += proportional;            // integral é o somatório dos erros
  last_proportional = proportional;    // last_proportional recebe o último erro

  float power_difference = ((proportional * p + integral * i + derivative * d) * ajuste) / (float) maxPWM; // A variável power_difference recebe o valor de correção da trajetória do robô em termos de velocidade,
  // a qual será adicionado em um motor e subtraído do outro

  MotorsPWMPercentual(vel + power_difference, vel - power_difference);  // Função para setar as velocidades e sentidos de rotação para ambos motores
}

void segueLinha(Control c) {                                    // Função que faz o controle do robô, a qual deve receber uma variável do tipo Control na qual há os parâmetros de controle
  unsigned int position = readLine(sensors, whiteLine);            // A variável position recebe o valor da posição do robô em relação a linha, variando entre 0 e 4000. Pela configuração atual o robô faz a leitura da sua posição em relação a uma linha branca em uma pista preta, caso queira que o robô leia uma linha uma linha preta sobre uma pista branca => unsigned int position = qtra.readLine(sensors);
  int proportional = position - 2000;                         // A variável proportional armazena o erro que vai de -2000 a 2000, uma vez que position varia entre 0 e 4000
  int derivative = proportional - last_proportional;          // A variável derivative recebe a variação do erro

  if ((proportional <= 0 && last_proportional >= 0) || (proportional >= 0 && last_proportional <= 0) || abs(proportional) == 2000) { // Se o robô estiver sobre a linha (proportional == 0) ou se ele passar sobre ela, a variável integral que é um somatório de todos os erros
    if (millis() - tempoErro > 3000) {
      integral = 0;
      tempoErro = millis();
      somaErros = 0;
    }                                                                                                                     // deve receber 0
  }

  integral += proportional;            // integral é o somatório dos erros
  last_proportional = proportional;    // last_proportional recebe o último erro

  float power_difference = ((proportional * c.Kp + integral * c.Ki + derivative * c.Kd) * ajuste) / (float) maxPWM; // A variável power_difference recebe o valor de correção da trajetória do robô em termos de velocidade,
  // a qual será adicionado em um motor e subtraído do outro

  MotorsPWMPercentual(c.vel + power_difference, c.vel - power_difference);  // Função para setar as velocidades e sentidos de rotação para ambos motores
}

void segueLinha(Control c, float vel) {                         // Função que faz o controle do robô, a qual deve receber uma variável do tipo Control e uma para velocidade
  unsigned int position = readLine(sensors, whiteLine);         // A variável position recebe o valor da posição do robô em relação a linha, variando entre 0 e 4000. Pela configuração atual o robô faz a leitura da sua posição em relação a uma linha branca em uma pista preta, caso queira que o robô leia uma linha uma linha preta sobre uma pista branca => unsigned int position = qtra.readLine(sensors);
  int proportional = position - 2000;                           // A variável proportional armazena o erro que vai de -2000 a 2000, uma vez que position varia entre 0 e 4000
  int derivative = proportional - last_proportional;            // A variável derivative recebe a variação do erro

  if ((proportional <= 0 && last_proportional >= 0) || (proportional >= 0 && last_proportional <= 0) || abs(proportional) == 2000) { // Se o robô estiver sobre a linha (proportional == 0) ou se ele passar sobre ela, a variável integral que é um somatório de todos os erros
    if (millis() - tempoErro > 3000) {
      integral = 0;
      tempoErro = millis();
      somaErros = 0;
    }
  }

  integral += proportional;            // integral é o somatório dos erros
  last_proportional = proportional;    // lastproportional recebe o último erro

  float power_difference = ((proportional * c.Kp + integral * c.Ki + derivative * c.Kd) * ajuste) / (float) maxPWM; // A variável power_difference recebe o valor de correção da trajetória do robô em termos de velocidade,
  // a qual será adicionado em um motor e subtraído do outro

  MotorsPWMPercentual(vel + power_difference, vel - power_difference);  // Função para setar as velocidades e sentidos de rotação para ambos motores
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * Ponte H, PWM * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Para controlarmos a velocidade e sentido de cada roda temos que usar a ponte H. Ela é impotante também por permitir a passa-
 * de tensão e corrente superiores ao alimentado pelo esp32. Dessa maneira, a energia que alimenta o motor de cada roda vem di-
 * retamente da bateria, evitando a ocorrência de queima do esp32.
 * Estamos usando a resolução de 12 bits para o PWM dos motores, permitindo selecionar uma potência entre 0 e 4095, com a frequên-
 * cia de 20000 Hz. A cada segundo, as portas responsáveis pelo PWM enviam uma onda quadra 20000 vezes para os motores. A frequên-
 * cia, segundo o datasheet da ponte H vermelha da pololu, deve ter um valor de no mínimo 5000 Hz. Isso se deve ao fato do PWM "se
 * comportar" um tensão analógica para frequências altas.
 */

//Define the Motors Pins

//Motor 1
int pinAIN1 = 16; //15; //5; // Direction
int pinAIN2 = 4;  //8;  //4; // Direction
int pinPWMA = 2;  //32; //3; // Speed

//Motor 2
int pinBIN1 = 5;  //0;  //7; // Direction
int pinBIN2 = 18; //4;  //8; // Direction
int pinPWMB = 19; //25; //9; // Speed

//Standby
int pinSTBY = 17; //2; //6;

//Constants to help remember the parameters of the motors
bool turnCW = false;
bool turnCCW = true;
bool motor1 = false;
bool motor2 = true;

void MotorsPWMPercentual(float m2, float m1) {   // Função que recebe os valores PWM para ambos motores, direito e esquerdo (valores entre -100.0 e 100.0)
  bool sentido1;                                 // O valor negativo inverte o sentido de rotação do motor
  bool sentido2;
  m1 = -m1;                                      // Pela forma que o robô foi montado, se mandarmos o mesmo PWM para ambos motores eles girarão em sentidos opostos, por isso deve-se inverter o sentido de um motor multiplicando por -1
  m1 = round(mapFloat(m1, -100.0, 100.0, -maxPWM, maxPWM));
  m2 = round(mapFloat(m2, -100.0, 100.0, -maxPWM, maxPWM));
  if (m1 < 0) {
    sentido1 = HIGH;
    m1 = -m1;
  } else sentido1 = LOW;

  if (m2 < 0) {
    sentido2 = HIGH;
    m2 = -m2;
  } else sentido2 = LOW;

  if (m1 > maxPWM) m1 = maxPWM;
  if (m2 > maxPWM) m2 = maxPWM;

  // Motor 1
  digitalWrite(pinAIN1, sentido1);
  digitalWrite(pinAIN2, !sentido1);  //This is the opposite of the AIN1
  //analogWrite(pinPWMA, m1);  // PWM do arduino
  ledcWrite(0, m1);            // PWM do esp32

  // Motor 2
  digitalWrite(pinBIN1, sentido2);
  digitalWrite(pinBIN2, !sentido2);  //This is the opposite of the BIN1
  //analogWrite(pinPWMB, m2);  // PWM do arduino
  ledcWrite(1, m2);            // PWM do esp32

  // Finally , make sure STBY is disabled - pull it HIGH
  digitalWrite(pinSTBY, HIGH);
}

void MotorsPWM(int m2, int m1) {   // Função que recebe os valores PWM para ambos motores, direito e esquerdo
  bool sentido1;                   // O valor negativo inverte o sentido de rotação do motor
  bool sentido2;
  m1 = -m1;                        // Pela forma que o robô foi montado, se mandarmos o mesmo PWM para ambos motores eles girarão em sentidos opostos, por isso deve-se inverter o sentido de um motor multiplicando por -1
  if (m1 < 0) {
    sentido1 = HIGH;
    m1 = -m1;
  } else sentido1 = LOW;

  if (m2 < 0) {
    sentido2 = HIGH;
    m2 = -m2;
  } else sentido2 = LOW;

  if (m1 > maxPWM) m1 = maxPWM;
  if (m2 > maxPWM) m2 = maxPWM;

  // Motor 1
  digitalWrite(pinAIN1, sentido1);
  digitalWrite(pinAIN2, !sentido1);  // This is the opposite of the AIN1
  //analogWrite(pinPWMA, m1);        // PWM do arduino
  ledcWrite(0, m1);                  // PWM do esp32

  // Motor 2
  digitalWrite(pinBIN1, sentido2);
  digitalWrite(pinBIN2, !sentido2);  // This is the opposite of the BIN1
  //analogWrite(pinPWMB, m2);        // PWM do arduino
  ledcWrite(1, m2);                  // PWM do esp32

  // Finally , make sure STBY is disabled - pull it HIGH
  digitalWrite(pinSTBY, HIGH);
}

// Freio -> ainda não está funcional

void freio(int tempo, float v) {
  int timeNow = millis();
  while (millis() - timeNow < tempo) {
    segueLinha(pidVel, v);
    delay(1);
  }
}

void motorsStop() {
  digitalWrite(pinAIN1, HIGH);
  digitalWrite(pinAIN2, HIGH);
  digitalWrite(pinBIN1, HIGH);
  digitalWrite(pinBIN2, HIGH);
}

void motorsStop(int tempo) {  // Ainda está bugado
  int timeNow = millis();
  motorsStop();
  while (millis() - timeNow < tempo);
}

void estrategia(int v) {                                  // Função responsável por executar os comandos da matriz estratégia
  for (int i = 0; i < linhas; i++) {                      // Ela é chamada quando o robô detecta a passagem por uma faixa lateral
    if (((int) matEstrategia[i][2]) == v) {               // esquerda ou direita
      switch ((int) matEstrategia[i][1]) {
        case 1: pidVel = reta;
          break;
        case 2: pidVel = curva;
          break;
        case 3: pidVel = retaRapida;
          break;
        case 4: pidVel = curvaFechada;
          break;
        case 5: pidVel = PID5;
          break;
      }
      velPID = matEstrategia[i][0];
      if ((int) matEstrategia[i][2] != 0) {
        timeBuzzer = millis();
      }
      if (matEstrategia[i][0] < 0) {
        freio(100, -100);
        velPID *= -1.0;
      }
      return;
    }
  }
}

void menuBotao() {
  int contTempo = 0;
  while (analogRead(bt) < 2047) delay(100); // Enquanto não pressionar o botão faça nada, apenas aguarde que alguém pressione
  do {
    contTempo++;
    delay(100);
  } while (analogRead(bt) > 2047 && contTempo <= 10);
  boolean sair = false;
  if (contTempo >= 10) {
    Serial.println("Entrou no menu, cont = ");
    Serial.print(contTempo);
    digitalWrite(ledVermelho, HIGH);
    digitalWrite(ledVerde,    HIGH);
    ledcWrite(channel, maxPWM);
    delay(100);
    ledcWrite(channel, 0);
    delay(100);
    ledcWrite(channel, maxPWM);
    delay(100);
    ledcWrite(channel, 0);
    digitalWrite(ledVermelho, LOW);
    digitalWrite(ledVerde,    LOW);
    while (analogRead(bt) > 2047) delay(100);
    contTempo = 0;
    int alternaCor = 0;
    while (!sair) {
      Serial.println("Entrou, cont: ");
      Serial.print(contTempo);
      Serial.println("\n");
      if (analogRead(bt) > 2047 && contTempo <= 10) {
        digitalWrite(ledVerde,    LOW);
        digitalWrite(ledVermelho, LOW);
        contTempo = 0;
        do {
          Serial.println("Contando");
          contTempo++;
          delay(50);
        } while (analogRead(bt) > 2047 && contTempo <= 10);
        if (contTempo >= 10) {
          sair = true;
          digitalWrite(ledVerde,    LOW);
          digitalWrite(ledVermelho, LOW);
          return;
        }
        Serial.println("Somando");
        selecionaPID++;
        //velPID += 1.0;
        alternaCor++;
        contTempo = 0;
        if (alternaCor % 2 == 0) {
          digitalWrite(ledVermelho, HIGH);
          digitalWrite(ledVerde,    LOW);
        } else {
          digitalWrite(ledVermelho, LOW);
          digitalWrite(ledVerde,    HIGH);
        }
        ledcWrite(channel, maxPWM);
        delay(100);
        ledcWrite(channel, 0);
        delay(100);
      }
    }
  }
  if (sair) {
    digitalWrite(ledVerde,    LOW);
    digitalWrite(ledVermelho, LOW);
  }
}

// Encoders

void encoders() {
  leituraEncoder[0][0] = digitalRead(pinEncoders[0]);
  leituraEncoder[0][1] = digitalRead(pinEncoders[1]);
  leituraEncoder[1][0] = digitalRead(pinEncoders[2]);
  leituraEncoder[1][1] = digitalRead(pinEncoders[3]);
}

void encEsqA() {
  if (digitalRead(pinEncoders[1]) != digitalRead(pinEncoders[0])) {
    contEncEsq ++;
  } else {
    contEncEsq --;
  }
  if(micros() - lastTimeEsq > 900) updateLeftEncoder();
}

float velEnc = 0;

void encDirA() {
  if (digitalRead(pinEncoders[3]) != digitalRead(pinEncoders[2])) {
    contEncDir ++;
  } else {
    contEncDir --;
  }
  if(micros() - lastTimeDir > 900) updateRightEncoder();
}

void updateLeftEncoder() {
  long dX = contEncEsq - lastcontEncEsq;
  lastcontEncEsq = contEncEsq;
  velEncEsq = dX*1000000.0/(60.0*(micros() - lastTimeEsq));
  velEnc = velEncEsq;
  lastTimeEsq = micros();
}

void updateRightEncoder() {
  long dX = contEncDir - lastcontEncDir;
  lastcontEncDir = contEncDir;
  velEncDir = dX*1000000.0/(60.0*(micros() - lastTimeDir));
  lastTimeDir = micros();
}

void setup() {
  Serial.begin(115200);

  //PIDs adaptados para a pista do linusbot
  reta.setControl(0.08, 0, 3.0);        // Inicializar respectivamente as variáveis Kp, Ki, Kd de controle e por último a velocidade
  curva.setControl(0.10, 0, 3.5);       // tanto para a reta quanto a curva
  retaRapida.setControl(0.15, 0, 6.0);
  curvaFechada.setControl(0.12, 0, 4.5); // Funcionarão como PID para v1, v2, v3, ..., vn onde vi cresce conforme o índice
  PID5.setControl(0.14, 0, 5.0);

  // Pinagem da ponte H
  pinMode(pinPWMA, OUTPUT);
  pinMode(pinAIN1, OUTPUT);
  pinMode(pinAIN2, OUTPUT);

  pinMode(pinPWMB, OUTPUT);
  pinMode(pinBIN1, OUTPUT);
  pinMode(pinBIN2, OUTPUT);

  pinMode(pinSTBY, OUTPUT);

  // Sensores frontais e laterais
  for (unsigned char i = 0; i < numSensors; i++) pinMode(portas[i], INPUT);
  pinMode(anaEsq, INPUT);
  pinMode(anaDir, INPUT);

  // Encoders
  for(unsigned char i = 0; i < 4; i++) {
    pinMode(pinEncoders[i], INPUT_PULLUP);
  }
  encoders();
  attachInterrupt(digitalPinToInterrupt(pinEncoders[0]), encEsqA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(pinEncoders[2]), encDirA, CHANGE);

  // Leds e botao
  pinMode(ledVermelho, OUTPUT);
  pinMode(ledVerde, OUTPUT);
  pinMode(bt, INPUT);

  digitalWrite(ledVermelho, HIGH);
  digitalWrite(ledVerde, LOW);

  // Buzzer
  ledcSetup(channel, freq, resolution);
  ledcAttachPin(pinBuzzer, channel);

  // Funções que fazem o papel de setar PWM no esp32
  ledcAttachPin(pinPWMA, 0); // Declara-se o pino e o canal referente ao
  ledcAttachPin(pinPWMB, 1); // PWM

  ledcSetup(0, freqPWM, resolucaoPWM);     // Para cada porta de PWM declarado deve-se setar o canal, a frequência dos pulsos por segundo
  ledcSetup(1, freqPWM, resolucaoPWM);     // e a "resolução" do PWM. O valor de 12 bits corresponde um valor de PWM de 0 a 4095

  //while (analogRead(bt) < 2047) delay(100);
  Serial.println("Botao");

  Serial.println("Comecou"); // Calibracao

  digitalWrite(ledVerde, HIGH);

  calibrarMedia(50);
  //calibrarMedia(200, 30, 5);
  Serial.println("Terminou");

  for (unsigned char i = 0; i < numSensors; i++) {
    Serial.print(i);
    Serial.print("  Min: ");
    Serial.print(maxMin[i][1]);
    Serial.print(", Max: ");
    Serial.print(maxMin[i][0]);
    Serial.print("\n\n\n");
  }
  Serial.print("Esquerdo");
  Serial.print("  Min: ");
  Serial.print(maxMinLateral[0][1]);
  Serial.print(", Max: ");
  Serial.print(maxMinLateral[0][0]);
  Serial.print("\n");
  Serial.print("Direito");
  Serial.print("  Min: ");
  Serial.print(maxMinLateral[1][1]);
  Serial.print(", Max: ");
  Serial.print(maxMinLateral[1][0]);
  Serial.print("\n\n\n");
  digitalWrite(ledVerde, LOW);

  //menuBotao();
  //pidVel = reta;
  //velPID = reta.vel;
  estrategia(selecionaPID);
  ledcWrite(channel, maxPWM);
  delay(200);
  ledcWrite(channel, 0);
  delay(300);
  //while(analogRead(bt) < 2047) delay(100);

  Serial.println("Botao, comeca a seguir a linha");

  digitalWrite(ledVerde, HIGH);
  digitalWrite(ledVermelho, LOW);

  delay(1000);

  ledcWriteTone(channel, 2075);
  ledcWrite(channel, 0);

  MotorsPWMPercentual(0, 0);
  inicio = millis();
  timeBuzzer = millis() + 300;
  lastTimeEsq = micros();
  lastTimeDir = micros();
}

void loop() {
  /*
  Serial.print(velEncEsq);
  Serial.print(" ");
  Serial.print(leftEncKalman.updateEstimate(velEnc));
  Serial.println();
  delay(1);
  */
  sensorEsquerdo();
  sensorDireito();
  if (millis() - timeBuzzer <= 100) ledcWrite(channel, maxPWM);
  else ledcWrite(channel, 0);
  if (contDir == 50) terminou = true;
  unsigned int minSensor = 1000;
  unsigned int maxSensor = 0;
  for (unsigned char i = 0; i < numSensors; i++) {
    if (sensors[i] > maxSensor) maxSensor = sensors[i];
    if (sensors[i] < minSensor) minSensor = sensors[i];
  }
  if (maxSensor - minSensor < 300 || minSensor > 200) {
    if (somaErros > erroMaximo) saiuDaLinha = true;
    somaErros++;
  }
  if (millis() - inicio > tempoLimite) terminou = true;
  if (saiuDaLinha) while (true) {
      MotorsPWM(0, 0);
      ledcWrite(channel, 0);
      digitalWrite(ledVermelho, HIGH);
      digitalWrite(ledVerde,     LOW);
      delay(800);
      digitalWrite(ledVermelho, LOW);
      digitalWrite(ledVerde,   HIGH);
      delay(200);
      digitalWrite(ledVermelho, HIGH);
      digitalWrite(ledVerde,    HIGH);
      delay(200);
      Serial.println("Saiu");
    } //else Serial.println(readLine(sensors, true) - 2000);
  if (!terminou) {
    segueLinha(pidVel, velPID);
  } else {
    unsigned long t = millis();
    while (millis() < t + 300) {
      segueLinha(reta, 20.0);
      delay(1);
      digitalWrite(ledVermelho, HIGH);
      digitalWrite(ledVerde, LOW);
    }
    contDir = 10000;
    ledcWrite(channel, 0);
    while (true) MotorsPWM(0, 0);
  }
  delay(1);
}
