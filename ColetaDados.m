clc; clear all; close all;
portaSerial='COM3';
dT=10;
if ~isempty(instrfind)
    fclose(instrfind);
    delete(instrfind);
end
if(~exist('arduino','var'))
    arduino = configSerial(portaSerial);
end
try
    nA= 50;
    r=5;
    for i = 1:r*nA
       if mod(i,5) ==  1
           u(i) = randi([0,255],1,1);
       else
           u(i) = u(i-1);
       end
    end
            for j=1:r*nA
                fprintf(arduino,u(i));
                %pause(0.1);
                d(i,1)=fscanf(arduino,'%d');
                %pause(0.1);
                d(i,2)=fscanf(arduino,'%u');
                %pause(0.1);
                d(i,3)=fscanf(arduino,'%u');
                %pause(0.1);
            end
            save('t100a01','d','dT');
            fclose(arduino);   
catch
    fclose(arduino);
end    